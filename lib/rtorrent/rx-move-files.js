'use strict';

module.exports = function(torrentData) {
    var Rx = require('rx'),
        path = require('path'),
        fsSync = require('./fs-sync');

    return Rx.Observable
        .create(observer => {
            var fileFilter = torrentData.common.config.rtorrent.fileFilter || false,
                source = torrentData.common.config.rtorrent.torrentDir + torrentData.torrent.name,
                dest = torrentData.common.config.rtorrent.targetDir + torrentData.filter.dir;

            if (fileFilter) {
                fsSync.walk(source).forEach(file => {
                    var baseName = path.basename(file);
                    observer.onNext(file);
                    baseName.match(new RegExp(fileFilter, 'i')) && fsSync.copyRecursive(file, path.join(dest, baseName));
                });
            } else {
                observer.onNext(source);
                fsSync.copyRecursive(source, path.join(dest, torrentData.torrent.name));
            }
            fsSync.rmDir(source);
            observer.onCompleted();
        })
        .toArray()
        .catch(error => {
            torrentData.common.log(error);
            return Rx.Observable.throw(error);
        });
};
