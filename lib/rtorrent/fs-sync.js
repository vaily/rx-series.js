'use strict';

var fs = require('fs'),
    path = require('path'),
    fsSync = function() {};

fsSync.prototype = {
    walk(dir, fileList = []) {
        if (!fs.statSync(dir).isDirectory()) {
            fileList.push(dir);
        } else {
            fs.readdirSync(dir).forEach(file => {
                var relName = path.join(dir, file);
                fileList = this.walk(relName, fileList);
            });
        }
        return fileList;
    },

    rmDir(dir) {
        fs.readdirSync(dir).forEach(file => {
            var relName = path.join(dir, file);
            if (fs.statSync(relName).isDirectory()) {
                this.rmDir(relName);
            } else {
                fs.unlinkSync(relName);
            }
        });
        fs.rmdirSync(dir);
    },

    copyFile(srcFile, destFile) {
        var BUF_LENGTH = 64 * 1024,
            buff = new Buffer(BUF_LENGTH),
            fdr = fs.openSync(srcFile, 'r'),
            fdw = fs.openSync(destFile, 'w'),
            bytesRead = 1,
            pos = 0;

        while (bytesRead > 0) {
            bytesRead = fs.readSync(fdr, buff, 0, BUF_LENGTH, pos);
            fs.writeSync(fdw, buff, 0, bytesRead);
            pos += bytesRead;
        }

        fs.closeSync(fdr);
        fs.closeSync(fdw);
    },

    copyRecursive(src, dest) {
        if (fs.statSync(src).isDirectory()) {
            fs.mkdirSync(dest);
            fs.readdirSync(src).forEach(file => this.copyRecursive(path.join(src, file), path.join(dest, file)));
        } else {
            this.copyFile(src, dest);
        }
    }
};

module.exports = new fsSync();
