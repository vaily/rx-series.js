'use strict';

module.exports = function(api) {
    var net = require('net');

    return function (observer, apiCall, apiParam) {
        var xml = '<?xml version="1.0"?><methodCall><methodName>' + apiCall + '</methodName><params><param><value><string>' + apiParam + '</string></value></param></params></methodCall>',
            ccZero = String.fromCharCode(0),
            head = 'CONTENT_LENGTH' + ccZero + xml.length + ccZero + 'SCGI' + ccZero + '1' + ccZero,
            respond = '',
            regex = /<value><[^>]+>([^<]+)<\/[^>]+><\/value>/gi,
            matches;

        net.connect(api.port, api.host)
            .on('error', e => observer.onError('{red}rTorrent API Error.{red} ' + e))
            .on('data', data => respond += data)
            .on('end', () => {
                while (matches = regex.exec(respond)) {
                    observer.onNext(matches[1]);
                }
            })
            .setEncoding('UTF8')
            .write(head.length + ':' + head + ',' + xml);
    }
};