'use strict';

module.exports = function(common) {
    var Rx = require('rx'),
        rtorrentApi = require('./rtorrent-api')(common.config.rtorrent.api);

    // get torrent list (result: torrent hashes)
    return Rx.Observable
        .create(observer => rtorrentApi(observer, 'download_list', ''))
        .flatMap(torrentHash => {
            // 3 more api calls for name, ratio and finished timestamp
            return Rx.Observable.combineLatest(
                Rx.Observable.just({hash: torrentHash}),
                ...['d.get_name', 'd.get_ratio', 'd.timestamp.finished'].map(apiCall =>
                    Rx.Observable.create(observer =>
                        rtorrentApi(observer, apiCall, torrentHash))
                            // rename keys: d.get_name -> name, d.get_ratio -> ratio, d.timestamp.finished -> finished
                            .map(apiResponse => ({[apiCall.match(/.*[_.](.*)$/).pop()]: apiResponse}))
                ))
                // merge the results into one object
                .map(results => Object.assign(...results))
                // filter for finished torrents
                .filter(data => data.finished > 0)
                .map(data => {
                    data.ratio /= 1000;
                    data.finished = Math.floor(Date.now() / 1000 - data.finished);
                    // add data and common data to the final result
                    return {
                        torrent: data,
                        common
                    };
                })
        })
        .catch(error => {
            common.log(error);
            return Rx.Observable.return('default value');
        })
};
