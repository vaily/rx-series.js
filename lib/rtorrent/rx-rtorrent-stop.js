'use strict';

module.exports = function(data) {
    var Rx = require('rx'),
        rtorrentApi = require('./rtorrent-api')(data.common.config.rtorrent.api),
        rxMoveFiles = require('./rx-move-files');

    Rx.Observable
        .create(observer => rtorrentApi(observer, 'd.erase', data.torrent.hash))
        .filter(response => response == 0) // torrent is succesfully stopped (response is 0)
        .flatMap(() => {
            var torrentName = '{magenta}' + data.torrent.name + '{/magenta}',
                torrentRatio = '{magenta}' + data.torrent.ratio + '{/magenta}',
                torrentSeedTime = '{magenta}' + Math.floor(data.torrent.finished / 3600) + ' hours{/magenta}';

            data.common.log('Stopped seeding ' + torrentName + ' (Ratio: ' + torrentRatio + ', seed time: ' + torrentSeedTime + ')');
            return rxMoveFiles(data);
        })
        .catch(error => Rx.Observable.return(false))
        .subscribe(result => {
            var dest = '{green}' + data.common.config.rtorrent.targetDir + data.filter.dir + '{/green}',
                source = '{bold}{green}' + data.torrent.name + '{/green}{/bold}';
            result && data.common.log('Moved {green}' + result.length + '{/green} files from ' + source + ' to ' + dest);
        });
};
