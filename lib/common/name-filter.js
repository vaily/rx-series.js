'use strict';

module.exports = function(name, filters) {
    var releaseNameParser = require('./release-name-parser'),
        releaseName = releaseNameParser(name),
        filterMatch = false;

    /*
    releaseName example:
    {
        "name": "Frequency",
        "season": 1,
        "episode": 1,
        "doubleEpisode": false,
        "releaseData": ".720p.HDTV.X264",
        "releaseGroup": "FLEET"
     }

     filter example:
     {
         "filter": "frequency",                         // filter for the name
         "releaseType": "^((?!.*\\.HUN[-.]).)*720p",    // optional
         "dir": "Frequency",                            // we don't need this
         "season": 1                                    // optional
     }
     */

    filters.forEach(filter => {
        if (releaseName && releaseName.name.match(new RegExp(filter.filter, 'i'))
                && (!filter.season || releaseName.season === filter.season)
                && (!filter.releaseType || releaseName.releaseData.match(new RegExp(filter.releaseType, 'i')))) {
            filterMatch = filter;
        }
    });

    return filterMatch;
};