'use strict';

module.exports = function (url, file) {
    var Rx = require('rx'),
        fs = require('fs'),
        urlParts = url.match(/http(s)?:\/\/([^/]+)(.*?)(?::COOKIE:(.*))?$/),
        secure = !!urlParts[1],
        cookie = urlParts[4] || null,
        options = {
            host: urlParts[2],
            path: urlParts[3],
            method: 'GET'
        },
        http = require(secure ? 'https' : 'http');

    cookie && Object.assign(options, {headers: {cookie}});

    return Rx.Observable.create(observer => {
        var response = '',
            req = http.request(options, function(res) {
                if (file) {
                    res.pipe(fs.createWriteStream(file));
                } else {
                    res.on('data', chunk => response += chunk);
                }
                res.on('end', () => {
                    observer.onNext(file ? true : response);
                    observer.onCompleted();
                });
            });

        req.on('error', error => observer.onError(error))
        req.end();
    });
};

