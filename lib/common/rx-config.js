'use strict';

var Rx = require('rx'),
    fs = require('fs'),
    rxLog = require('./rx-log'),
    Database = require('./database'),
    readConfig = function(observer, configFile) {
        var common,
            config,
            json = fs.readFileSync(configFile);

        try {
            config = JSON.parse(json);
            common = {
                config,
                log: rxLog,
                db: new Database(config.sqlite.dbFile)

            };
            observer.onNext(common);
        } catch (err) {
            observer.onError(err);
        }

        return common;
    };

module.exports = function(configFile) {
    return Rx.Observable.create(observer => {
        var common = readConfig(observer, configFile);
        common.log('{yellow}Config file is loaded.{/yellow}')
        common.config.watchConfig && fs.watchFile(configFile, {persistent: true, interval: common.config.watchConfig}, () => {
            common = readConfig(observer, configFile);
            common.log('{yellow}Config file is changed.{/yellow}');
        });
    });
};
