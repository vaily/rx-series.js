'use strict';

/**
 * @param configFile
 * @returns Observable
 */
module.exports = function(configFile, tickObservable, moduleName) {
    var Rx = require('rx'),
        rxConfig = require('./rx-config');

    return Rx.Observable
        .interval(1000)
        .combineLatest(rxConfig(configFile), (tick, common) => ({tick, common}))
        .distinct(data => data.tick) // tick will be duplicated on config change
        .flatMap(data => (data.tick % data.common.config[moduleName].checkInterval ?
            Rx.Observable.empty() :
            tickObservable(data.common)
        ));
};
