'use strict';

/**
 * @param releaseName {String}
 * @returns {Object} Returns with episode information or null
 */
module.exports = function(releaseName) {
    var matches = releaseName.match(/(.*)[. _](?:S([0-9]+).?E([0-9]+)(?:[E-]([0-9]+))?|([0-9])+x([0-9]+)(?:-([0-9]+))?)(.*?)-([^.]+)$/),
        release = !matches ? matches : {
            name: matches[1].replace(/[._]/g, ' '),
            season: parseInt(matches[2] || matches[5], 10),
            episode: parseInt(matches[3] || matches[6], 10),
            doubleEpisode: (matches[4] || matches[7]) ? true : false,
            releaseData: matches[8],
            releaseGroup: matches[9]
        };
    return release;
};
