'use strict';
var styles = {};
module['exports'] = styles;

var Rx = require('rx'),
    subject = new Rx.Subject(),
    rxLog,
    colorCodes = {
        reset: [0, 0],

        bold: [1, 22],
        dim: [2, 22],
        italic: [3, 23],
        underline: [4, 24],
        inverse: [7, 27],
        hidden: [8, 28],
        strike: [9, 29],

        black: [30, 39],
        red: [31, 39],
        green: [32, 39],
        yellow: [33, 39],
        blue: [34, 39],
        magenta: [35, 39],
        cyan: [36, 39],
        white: [37, 39],
        grey: [90, 39]
    };

rxLog = subject
    .map(log => {
        typeof log === 'string' && Object.keys(colorCodes).forEach(color => {
            log = log.replace(new RegExp('{' + color + '}', 'ig'), '\u001b[' + colorCodes[color][0] + 'm');
            log = log.replace(new RegExp('{/' + color + '}', 'ig'), '\u001b[' + colorCodes[color][1] + 'm');
        });
        return log;
    })
    .map(log => {
        var text = new Date().toISOString().replace(/([0-9-]+)T([0-9:]+).*/, '$1 $2 - ');
        return text + log;
    })
    .distinctUntilChanged();

rxLog.subscribe(log => {
    console.log(log);
});

module.exports = function(log) {
    subject.onNext(log);
};
