'use strict';

var fs = require('fs'),
    sqlite3 = require('sqlite3'),
    Rx = require('rx'),
    Database = function(dbFile) {
        this.db = null;
        this.initDb(dbFile);
    };


Database.prototype = {
    initDb(dbFile) {
        var exists = fs.existsSync(dbFile);
        this.db = new sqlite3.Database(dbFile);
        !exists && this.db.serialize(() => this.db.run('CREATE TABLE downloaded (item TEXT, done BOOLEAN DEFAULT false)'));
        this.rxQuery('SELECT count(*) FROM downloaded').subscribe();
    },

    rxQuery(query, params = {}) {
        return Rx.Observable.create(observer =>
            this.db.each(query, params, (err, row) => {
                if (err) {
                    console.log('SQLite DB error: ' + err.message);
                    process.exit(1);
                }
                observer.onNext(row);
            })
        );
    },

    isRssItemDownloaded(title) {
        return this.rxQuery('SELECT count(*) as cnt FROM downloaded WHERE item = ?', title).map(result => !!result.cnt);
    },

    setRssItemDownloaded(title) {
        return this.rxQuery('INSERT INTO downloaded (item) VALUES(?)', title).map(result => !!result.cnt);
    }

};

module.exports = Database;