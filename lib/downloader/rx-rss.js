'use strict';

module.exports = function(common) {
    var rxFetchUrl = require('./../common/rx-request'),
        xml2json = require('simple-xml2json'),
        Rx = require('rx');

    return Rx.Observable
        .fromArray(common.config.rss.feeds)
        .flatMap(feed => {
            return rxFetchUrl(feed.url)
                .map(xml => xml2json.parser(xml))
                .filter(json => !!json.rss && !!json.rss.channel)
                .flatMap(json => json.rss.channel.item)
                .map(item => {
                    var urlCookie = feed.url.match(/(:COOKIE:.*)/);
                    urlCookie && (item.link += urlCookie[1]);
                    return {
                        feedItem: {
                            title: feed.titleRegex ? item.title.match(feed.titleRegex)[1] : item.title,
                            link: item.link.replace('&amp;', '&'),
                            site: feed.name
                        },
                        common
                    }
                })
                .catch(error => {
                    throw({
                        error,
                        feed: feed.name,
                        message: '{red}Couldn\'t download {bold}' + feed.name + '{/bold} RSS feed.{/red} ' + error
                    });

                });
        })
        .distinct(item => item.feedItem.title)
        .catch(error => {
            common.log(error.message || error);
            return Rx.Observable.return('default value');
        })
};
