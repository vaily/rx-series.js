'use strict';

module.exports = function(configFile) {
    var rxTicker = require('./common/rx-ticker'),
        rxRss = require('./downloader/rx-rss'),
        rxRequest = require('./common/rx-request'),
        nameFilter = require('./common/name-filter'),
        path = require('path');

rxTicker(configFile, rxRss, 'rss')
    // filter for rss item object on the stream
    .filter(data => data.feedItem)
    // apply filters in config.json, add source and target dirs for file moving
    .map(data => !nameFilter(data.feedItem.title, data.common.config.filters) ? null : {
        common: data.common,
        item: data.feedItem
    })
    // remove non-objects (return null in previous map)
    .filter(data => data)
    // is the item already downloaded?
    .flatMap(data =>
        data.common.db.isRssItemDownloaded(data.item.title)
            .map(result => Object.assign(data, {downloaded: result}))
    )
    .filter(data => !data.downloaded)
    // download the torrent file
    .flatMap(data => rxRequest(data.item.link, path.join(data.common.config.rss.downloadDir, data.item.title) + '.torrent')
        .map(result => {
            // add item to the db
            result && data.common.db.setRssItemDownloaded(data.item.title).subscribe();
            return Object.assign(data, {result})
        })
    )
    // result will be true on a succesful download
    .filter(data => data.result)
    // add row to database with item title
    .subscribe(data => {
        data.common.log('Downloaded: {blue}{bold}' + data.item.title + '{/bold}{/blue} from ' + data.item.site);
    });
};

