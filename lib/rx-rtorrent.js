'use strict';

module.exports = function(configFile) {
    var rxTicker = require('./common/rx-ticker'),
        rxRtorrentSeedList = require('./rtorrent/rx-rtorrent-seed-list'),
        rxRtorrentStop = require('./rtorrent/rx-rtorrent-stop'),
        nameFilter = require('./common/name-filter');

    rxTicker(configFile, rxRtorrentSeedList, 'rtorrent')
        // filter for torrent seed object on the stream
        .filter(data => data.torrent)
        // filter for seed time and ratio
        .filter(data => data.torrent.ratio > data.common.config.rtorrent.minSeedRatio || data.torrent.finished > data.common.config.rtorrent.minSeedHours * 3600)
        // apply filters in config.json, add source and target dirs for file moving
        .map(data => {
            var filter = nameFilter(data.torrent.name, data.common.config.filters);
            return filter ? {
                common: data.common,
                torrent: data.torrent,
                filter
            } : null;
        })
        // remove non-objects (return null in previous map)
        .filter(data => data)
        .subscribe(data => rxRtorrentStop(data));
};