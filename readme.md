# Series.js

Requirements:
- rtorrent with xmlrpc support (in .rtorrent.rc: `scgi_port = localhost:5000`)
- access to a torrent site :)

## Installing

- ```npm install```
- create and edit the ```series.json``` file

## series.json

```json
{
    // time milliseconds. It Checks the series.json file, updates the config on the fly when it changes
    "watchConfig": 2000,

    // RSS downloader configs
    "rss": {
        // RSS feed check interval in seconds
        "checkInterval": 600,
        // array, rss feeds
        "feeds": [
            {
                // feed name, string, for local use, can be anything
                "name": "Site1",
                // feed url. cookies can be passed by adding :COOKIE:cookie=value;cookie2=value2;...
                "url": "https://torrentsite1.tld/rss/rssdd.xml:COOKIE:nick=XXXXXX;pass=XXXXXXXXXXXX",
                // optional: a regex for getting torrent name from the titles
                // example: "Westworld.S01E02.720p.HDTV.x264-BATV [TV-HD]"
                "titleRegex": "(.*) \\["
            },
            {
                // more than one rss feed can be added
                "name": "Site2",
                "url": "https://torrentsite2.tld/rss?feed=dl&cat=27&passkey=XXXXXXXXXXXX"
            }
        ],
        // rtorrent watch directory path
        // can be set in .rtorrent.rc, for example:
        // schedule = watch_directory,5,5,load_start=./watch/*.torrent
        "downloadDir": "/path/to/torrent/watch-dir/"
    }
    // SQLite configs
    "sqlite": {
        // name of the sqlite db file
        "dbFile": "series.sqlite"
    },
    // rtorrent configs
    "rtorrent": {
        // rtorrent check interval in seconds
        "checkInterval": 600,
        // host and port for rtorrent xmlrpc api
        "api": {
            "host": "127.0.0.1",
            "port": 5000
        },
        // rtorrent download directory
        // in .rtorrent.rc:
        // directory = relative_path_to_dl_dir
        "torrentDir": "/path/to/torrent/download-dir/",
        // target dir, files will be moved here when download and seeding both finished
        "targetDir": "/path/to/final-dir",
        // seed settings. seeding will be stopped when ratio OR seed time is reached
        // minimum seed ratio
        "minSeedRatio": 3,
        // minimum seed time in hours
        "minSeedHours": 24,
        // optional: regex filter for the files, that you need. *ALL other files will be deleted*
        // without providing this, all files will be moved to the target dir recursively
        // example below: all mkv, rar, and r01, r02 etc files
        "fileFilter": ".*\\.(?:mkv|rar|r[0-9]+)"
    },

    // series filters
    "filters": [
        {
            // filter for the tv show name.
            // it is for the name only, e.g. The.Blacklist in The.Blacklist.S04E02.720p.HDTV.X264-DIMENSION
            "filter": "the.blacklist",
            // optional: regex filter for the release info. 720p.HDTV.X264-DIMENSION in the example above
            // in this example: it should be a 720p and non hungarian release
            "releaseType": "^((?!.*[-.]HUN[-.]).)*720p",
            // Relative directory from the target path (targetDir above in rtorrent settings)
            "dir": "The.Blacklist",
            // season filter. a specific season or 0 for any season
            "season": 4
        },
        {
            "filter": "vikings",
            "dir": "Vikings",
            // any season
            "season": 0
        },
        {
            "filter": "frequency",
            // 720p only
            "releaseType": "720p",
            "dir": "Frequency",
            // first season only
            "season": 1
        }
    ]
}
```