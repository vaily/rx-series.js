'use strict';

var configFile = './series.json',
    rxRtorrent = require('./lib/rx-rtorrent'),
    rxDownloader = require('./lib/rx-downloader');

rxRtorrent(configFile);
rxDownloader(configFile);
